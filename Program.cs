using System;

interface ICarFactory
{
    IEngine CreateEngine();
    IBody CreateBody();
}

class SedanFactory : ICarFactory
{
    public IEngine CreateEngine()
    {
        return new GasolineEngine();
    }

    public IBody CreateBody()
    {
        return new SedanBody();
    }
}

interface IEngine
{
    void Start();
}

class GasolineEngine : IEngine
{
    public void Start()
    {
        Console.WriteLine("Запуск бензинового двигателя");
    }
}

interface IBody
{
    void Assemble();
}

class SedanBody : IBody
{
    public void Assemble()
    {
        Console.WriteLine("Сборка кузова седана");
    }
}

class Car
{
    private IEngine _engine;
    private IBody _body;
    private string _interior;

    private ICarFactory _factory;

    public Car(ICarFactory factory)
    {
        _factory = factory;
        _engine = _factory.CreateEngine();
        _body = _factory.CreateBody();
        _interior = "стандартный салон";
    }

    public virtual void Assemble()
    {
        _body.Assemble();
    }

    public virtual void Start()
    {
        _engine.Start();
    }

    public void SetInterior(string interior)
    {
        _interior = interior;
    }

    public string GetInterior()
    {
        return _interior;
    }

    public virtual ICarFactory GetFactory()
    {
        return _factory;
    }
}

abstract class CarDecorator : Car
{
    protected Car _car;

    public CarDecorator(Car car) : base(car.GetFactory())
    {
        _car = car;
    }

    public override void Assemble()
    {
        _car.Assemble();
    }

    public override void Start()
    {
        _car.Start();
    }

    public abstract string GetInterior();

    public override ICarFactory GetFactory()
    {
        return _car.GetFactory();
    }
}

class LeatherInterior : CarDecorator
{
    public LeatherInterior(Car car) : base(car)
    {
    }

    public override void Assemble()
    {
        base.Assemble();
        Console.WriteLine("Установка кожаного салона");
    }

    public override string GetInterior()
    {
        return _car.GetInterior() + ", кожаный салон";
    }
}

class Program
{
    static void Main(string[] args)
    {
        ICarFactory factory = new SedanFactory();

        Car car = new Car(factory);
        car.Assemble();
        car.Start();
        Console.WriteLine(car.GetInterior());

        CarDecorator carWithLeatherInterior = new LeatherInterior(car);
        carWithLeatherInterior.Assemble();
        carWithLeatherInterior.SetInterior(carWithLeatherInterior.GetInterior());
        carWithLeatherInterior.Start();
        Console.WriteLine(carWithLeatherInterior.GetInterior());
    }
}
